/****************************************************
 * file    : clock.c
 * project : qr347 (8-step Midi Sequencer)
 * target  : ATmega328P (16MHz)
 * author  : ./qusw (https://github.com/qusvv)
 * license : GNU GPLv3 
 * 
 * Midi IN / CLOCK generation for QR347
 *  
 * ./qusw
****************************************************/

#include "inc/clock.h"


/* -----INTERNAL CLOCK-------*/

volatile uint16_t t=0;   // time in tenths of milliseconds (@10kHz)
extern volatile uint16_t clk; // number of ticks since begining of first step.
extern volatile uint16_t Tclk;      //Pulse length in tenths of milliseconds

void init_timer0(void){ /* Set up TIMER0 at fHz=10kHz (CTC mode) */
	/*  F_CPU = 16MHz ; fHz=10kHz ; Pre = 8
	 * 	OCR0A = [F_CPU/(fHz*Pre) -1] = [16*10^6/(10*10^3*8) -1] = 199 <256!!
	 */
	cli(); 						// disable global interrupts
	TCCR0A = 0;					// set entire TCCR0A register to 0
	TCCR0B = 0;					// same for TCCR0B
	TCNT0  = 0;					// initialize counter value to 0	
	OCR0A = 199;  				// set Compare Register (see comment above)
	TCCR0A |= (1 << WGM01);		// clear timer on compare match (CTC mode on)
	TCCR0B |= (1 << CS01);   	// 8 prescaler
	TIMSK0 |= (1 << OCIE0A); 	// enable interrupts on TIMER0 (COMPA)
	sei();						// enable global interrupts
}

ISR(TIMER0_COMPA_vect){ //TIMER0 @ 10kHz (0.1ms)
	t++;
	if(100*t>Tclk){ // Tick "received" (Tclk:10^-3 ms  vs t:10^-1 ms)
		clk++;
		t=0;
	}
}

void update_clk(uint8_t tempo){
	Tclk=(uint16_t)(60.0*(1000.0/tempo)*(1000.0/PPQN));
}

