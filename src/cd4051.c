/****************************************************
 * file   : cd4051.c
 * project : qr347 (8-step Midi Sequencer)
 * target  : ATmega328P (16MHz)
 * author  : ./qusw (https://github.com/qusvv)
 * license : GNU GPLv3 
 * 
 *  LED/Pots multiplexer (cd4051)
 *  
 * ./qusw
****************************************************/
#include "inc/cd4051.h"

void init_4051(void){ 
	DDR_ABC |= (1<<PIN_A) | (1<<PIN_B) | (1<<PIN_C); //set A,B,C pins as output
	DDR_LED |= (1<<PIN_LED);  //LED COM as output
	PRT_LED |= (1<<PIN_LED);  //LED high by default
}

void set_OUT(volatile uint8_t *port, uint8_t pin, uint8_t state){ //set an output pin to HIGH/LOW
	if (state){*port |= (1<<pin);} // HIGH = 1 = ON
	else{*port &= ~(1<<pin);} //LOW = 0 = OFF
}
//NB: see AVR Libc Reference Manual : How to pass an IO port as a parameter
	
void set_ABC(uint8_t i) { //set the 4051 A,B,C input states for a given step (0<= i < 8)
	set_OUT(&PRT_ABC, PIN_A, (i & 0x01) );  // NB: the adress of the port is passed here
	set_OUT(&PRT_ABC, PIN_B, ((i>>1) & 0x01) );
	set_OUT(&PRT_ABC, PIN_C, ((i>>2) & 0x01) );	
}
