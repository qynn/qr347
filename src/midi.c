/***************************************************************
 * file    : midi.h  (header for midi.c)
 * project : qr347 (8-step Midi Sequencer)
 * target  : ATmega328P (16MHz)
 * author  : ./qusw (https://github.com/qusvv)
 * license : GNU GPLv3 
 * 
 * Minimal MIDI Serial UART fir ATmega328P (16MHz)
 *  
 * ./qusw
****************************************************************/
   
#include "inc/midi.h"

#ifndef F_CPU
#define F_CPU 16000000UL
#endif
#define BAUD 31250

// BaudRate Expression for double speed Mode : 
// UBBR = FCPU/8BAUD -1 +0.5 (rounded)
//#define UBRRV (( (F_CPU) + 4UL * (BAUD) ) / (8UL * (BAUD) ) - 1UL) 

// BaudRate Expression for normal asynchronous Mode : 
// UBBR = FCPU/16BAUD -1 +0.5 (rounded)
#define UBRRV (( (F_CPU) + 8UL * (BAUD) ) / (16UL * (BAUD) ) - 1UL) 
/*alternately check  #include <util/setbaud.h> */

void init_uart(void) {
    UBRR0H = (UBRRV>>8);     			// UBBRV MSB
    UBRR0L = (UBRRV& 0xff); 			// UBBRV LSB
    //UCSR0A |= (1<<U2X0); 			// using 2X speed
    UCSR0C |= (1<<UCSZ01) | (1<<UCSZ00); 	// 8N1 (sync) mode 
    UCSR0B |= (1<<TXEN0);   			// enabale TX 
    
}

void send_midi(uint8_t MIDIcmd, uint8_t MIDInot, uint8_t MIDIvel){
	send_byte(MIDIcmd);	//Command
	send_byte(MIDInot);	//Pitch
	send_byte(MIDIvel);	//Velocity
}

uint8_t note_ON(uint8_t MIDIch, uint8_t MIDInot, uint8_t MIDIvel){
    uint8_t MIDIcmd = NOTE_ON + MIDIch -1;
    send_midi(MIDIcmd,MIDInot,MIDIvel);
    return MIDInot;
}

void note_OFF(uint8_t MIDIch, uint8_t MIDInot, uint8_t MIDIvel){
    uint8_t MIDIcmd= NOTE_OFF + MIDIch -1;
    send_midi(MIDIcmd,MIDInot,MIDIvel);
}

void send_byte(uint8_t b) {
    wait_for_flag(UCSR0A, UDRE0); /* Wait until data register is empty. */
    UDR0 = b;
} /*alternately wait until transmission is ready: wait_for_flag(UCSR0A,TXC0)*/ 


