/******************************************************************************
 * file    : sofTx.c  (Send-Only Software UART)
 * target  : ATmega328P (16MHz)
 * author  : ./qusw (https://github.com/qusvv)
 * license : GNU GPLv3
 * 
 * A minimal bit bang, interrupt driven UA(R)T (Transmit Only) for ATmega328P (16MHz)
 * Largely inspired by SoftwareUART from Jeroen Lodder (2011)
 * 
 * NB1: NewSoftSerial (Mikal Hart) and SoftwareSerial use respectively a tunedDelay() function 
 * written in assembly code and _delay_loop_2 from <utili/delay_basic.h> to adjust the timing 
 * between two consecutive bytes. This way, max. resolution is 4 CPU cycles (250ns @ 8MHz)
 * Comparatively, when using TIMER0 a 125ns resolution can be achieved by adjusting 
 * the compare match register (prescaler =1)
 * 
 * NB2: SoftwareSerial substracts about 15 cycles from bit delay to take into acount for
 * code execution. This is not done here. Rather, a switch structure is used to make sure 
 * delays between: 
 * - Start bit and first data bit.
 * - two consecutive data bits
 * - last data bit and stop bit
 * are all kept at pretty much the same value. 
 
 * 
 * ./qusw
******************************************************************************/
#include "sofTx.h"
#include "debug.h"
#include <avr/io.h>
#include <avr/interrupt.h>

//Variables:
volatile uint8_t	TXstep	 = 0;		// 8N1 step counter (0=PAUSE; 1=START; 2-9=DATA; 10=STOP)
volatile uint8_t 	TXdata 	= 0;		// Data byte

void init_TX(void){ 		/* init TX on TIMER1 (CTC mode)*/
	
	cli(); 					//disable global interrupts
	DDR_TX	|=	(1<<PIN_TX);	//TXpin set as Output in DDRD (Data Direction Register)
	PRT_TX	|=	(1<<PIN_TX); //initialize at high state
	TCCR1A = 0;				// set entire TCCR0A register to 0
	TCCR1B = 0;				// same for TCCR0B
	TCNT1  = 0;				// initialize counter value to 0
	 
	 /* 
	 **FTDI:
	 * fHz=115200 ; Pre = 1 
	 * OCR1A = [F_CPU/(fHz*Pre) -1] = [16*10^6/(115200) -1] ~ 138 (+/-1) >> ?? OK!
	 * 
	 * */
	 
	OCR1A = 138;			// see comment above
	TCCR1B |= (1 << WGM12); // turn on CTC mode (on OCR1A)
	TCCR1B |= (1 << CS10);  // no prescaler 
	TIMSK1  |= (1 << OCIE1A);// enable timer interrupt
	sei();					//enable interrupts
}

ISR(TIMER1_COMPA_vect){		//Interrupt on Timer1
	
	switch(TXstep){
		//##########
		case 0:											    //IDLE
			 TIMSK1 &= ~(1<<OCIE1A);						//disable TIMER1
			 TIMSK0 |= (1<<OCIE0A);    						//re-enable TIMER0
			break; 	//Wait until the next SEND request
		//##########
		case 1:												//START bit
			PRT_TX  &= ~(1<<PIN_TX);							//Pull down (LOW)
			TXstep++;									    //step increment
			break;	//Wait (1 bit) then goes to DATA Mode
		//##########
		case 2: case 3: case 4: case 5: case 6: case 7: case 8: case 9:											//DATA Mode (8 bits)
				if( TXdata & 0x01) PRT_TX |= (1<<PIN_TX);		//Pull up (HIGH)
				else PRT_TX  &= ~(1<<PIN_TX);					//Pull down (LOW)
				TXdata >>= 1;								//Shift Data byte once to the right
				TXstep++;								//Increment byte counter						
				break;	//Wait (1bit)
		//##########
		case 10:											//STOP bit								
			PRT_TX |= (1<<PIN_TX);							//Pull up (HIGH)
			TXstep  = 0;									//Reset step counter
			break; // goes to PAUSE mode
		//##########
		default:
			TXstep 	= 10; // STOP bit (HIGH) then PAUSE by default
			break;
	}
	return;
}

void stx_byte(uint8_t B){	    //1 data byte (or 1 ASCII caracter)
	while(TXstep > 0){
		//Wait until previous message has been sent
		}; 
		TXdata = B;					//copy data to be sent
		TXstep = 1; 				//START bit Case
		TIMSK0 	&= ~(1<<OCIE0A);    //disable TIMER0 to avoid delays
		TIMSK1 	|=  (1<<OCIE1A);	//re-enable TIMER1
		
		/*data byte will be sent from the Interrupt Service Routine
		 * and TXstep will go back to 0 when job is done
		 */
}

void stx_int(int a){	
	uint8_t lgth=6;
	char buf[lgth]; //5 digits + nul character ('\0')
	char *str=&buf[lgth-1]; //now points to last character
	*str ='\0'; //nul character to terminate string
	do{
		char c = a % 10;
		*(--str)= c + '0'; //ascii digits start at '0'=48;
		a/=10;
	}while(a);
	
	stx_str(str);
}
	
void stx_str(char *s){
	uint8_t i= 0; //counter 
	
	stx_byte('\0'); //send dummy byte to avoid errors on first character
	while ( (s[i] != '\0') & (i < STR_MAX) ) //end of string  	
	{
		stx_byte(s[i]);
		i++;
	}
}




