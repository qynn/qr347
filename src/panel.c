/*******************************************************************
 * file    : panel.c
 * project : qr347 (8-step Midi Sequencer)
 * target  : ATmega328P (16MHz)
 * author  : ./qusw (https://github.com/qusvv)
 * license : GNU GPLv3 
 * 
 * Functions for reading Pot/Switch values on front panel
 *  
 * ./qusw
********************************************************************/

#include "inc/panel.h"



/******* ADCs ************/

const uint8_t R347_pot[11]={
	0,5,25,58,88,
	117,140,167,
	198,229,251};
	
const uint8_t R347_val[11]={ //retrieve 12
	99,0,3,4,7,
	12,15,16,19,
	24,27};


uint8_t getTempo(void){
	set_MPX(CH_T);
	uint8_t pot=read_ADC_8bit(); //0-255
	uint8_t temp=TEMPO_MIN+((uint16_t)((TEMPO_MAX-TEMPO_MIN+1)*pot)>>8) ; //BPM
	return temp;
}

uint8_t switch3(void){
	uint8_t sw3=read_ADC_8bit(); //0-255
	uint8_t pos=1;
	if (sw3>251){pos=4;}
	else if (sw3<5){pos=8;}
	else {pos=16;}
	return pos;
}

uint8_t getRate(void){
	set_MPX(CH_R);
	return switch3();
}

uint8_t getQuantize(void){
	set_MPX(CH_Q);
	return switch3();
}

uint8_t getGate(void){
	set_MPX(CH_G);
	uint8_t g=read_ADC_8bit(); //0-255
	return g;
}

uint8_t getR347(void){
	set_MPX(CH_C);		//4051 COM
	uint8_t c=read_ADC_8bit(); //0-255
	uint8_t n=0;
	for (uint8_t i=0; i<11; i++){
		if ( R347_pot[i]<=c ) {n=i;}
	}
	return R347_val[n];
}

uint8_t getVelocity(void){
	set_MPX(CH_V);
	uint8_t pot=read_ADC_8bit(); //0-255
	uint8_t v=pot>>1; //0-127
	return v;
}

/******* ON-OFF Switches ************/

void init_SW(void){ // set SW pins as input
	DDR_SW &= ~(1<<SW_S) & ~(1<<SW_C) & ~(1<<SW_B) & ~(1<<SW_R);
}


uint8_t check_SW(uint8_t swPin){  //  0V/5V on swPin (PB0)
	if ( !(PRT_SW & (1<<swPin)) ){  //Pin to GND
		return 0;
		}
	else{ return 1;}
}

