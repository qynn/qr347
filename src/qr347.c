/****************************************************************
 * file    : qr347.h  (main header file)
 * project : qr347 (8-step Midi Sequencer)
 * target  : ATmega328P (16MHz)
 * author  : ./qusw (https://github.com/qusvv)
 * license : GNU GPLv3
 * 
 * ./qusw
****************************************************************/

//#include "inc/debug.h"
//#include "inc/sofTx.h"
#include "inc/qr347.h"

#include <util/delay.h>
#include <avr/eeprom.h>

/*** DATA stored in EEPROM ***/
uint8_t mco,MCO; //Midi Channel OUT, EEPROM
uint8_t mci,MCI; //Midi Channel IN, EEPROM
uint8_t src,SRC; //Clock Source (1-2), EEPROM

/*** TOP PANEL ***/
uint8_t temp=120; 	// (T)empo (BPM)
uint8_t clck=0;		// (C)lock (0:int ; 1:ext)
uint8_t hold=0;		// (S):Hold (0:2(OFF) ; 1: 1(ON))
uint8_t rate=8; 	// (R)ate (4-8-16)
uint8_t qutz=250;   // (Q)uantize (4-8-16)
uint8_t gate=127;   // (G)Gate (0-255)
uint8_t bpss=0;   	// (B)ypass (0:OFF ; 1:ON)
uint8_t velo=100;  	// (V)elocity (0-127)

/*** TIMING parameters (c.f. TIMER0 (10kHz) in  clock.c) ***/
volatile uint16_t clk; // number of ticks since begining of step.
uint16_t Tclk=3000;  //Pulse length in microseconds (us)
uint8_t step=0; //current step (0-7)
uint8_t Ntck = 48; //Ticks per Step	
/*NB: uint8_t Ntck= (PPQN >>(rate>>3)) 
Rate=4 : Nticks = PPQN 			: 1 step per Quarter note
Rate=8 : Nticks = PPQN >> 1 	: 2 steps per Quarter note
Rate=16 : Nticks = PPQN >>2		: 4 steps per Quarter note	 
*/

/*** NOTES ***/
uint8_t root_cur=45; //current root
uint8_t root_nxt=50; //next root
uint8_t note_cur=48; //current note
uint8_t note_nxt=38; //next note
uint8_t note_snt=52; //note sent
uint8_t r347=0;	  // next pot value

/*DEBUG
void long_delay(uint16_t ms) {
	for(; ms>0; ms--)
		_delay_ms(1);
}
*/

int main(){
	
	init_4051(); //LED/Pots multiplexer
	init_timer0(); //Internal clock
	init_ADC();  //Analog to Digital Converters
	init_TX();	// Software Serial Tx
	init_SW();	// ON-OFF Switches
	init_uart(); // MIDI Serial
	
	getMidiSetup(); //Retrieve MIDI channels/source from EEPROM/SETUP
	update_ALL(); // init all parameters
	
	while(1){
		
		if(clk>Ntck){ //New Step reached
			clk=clk-Ntck;	//update clock
			if(++step==8){ //increment
				step=0; //init (end of line)
				}
			note_cur=note_nxt;   // update note
			set_ABC(step);  // update 4051 Index
			if( (note_cur>0) & !((note_cur==note_snt)&(gate>250)) ){
			/* NB: Note is NOT sent if:
			 * i. next pot is OFF(0)
			 * OR
			 * ii. (next note is the same) AND (Gate is MAX) */ 
				note_snt=note_ON(mco,note_cur,velo); //SEND MIDI NOTE
				set_OUT(&PRT_LED, PIN_LED, 1); 		// TURN LED ON
				stx_str("ON:");
				stx_int(note_snt);
				stx_str("\r\n");
			}
			else{ //SKIP STEP
				stx_str("CH_IN");
				stx_int(mci);
				stx_str("_OUT");
				stx_int(mco);
				stx_str("_SRC");
				stx_int(src);
				stx_str("\r\n");
				}

			note_nxt=getNextNote();  // read next Note 
			
			// Check Panel Parameters
			if (step==7){ update_CHRQB();} // update SWITCHES at the end of line
			else{  update_TGV();}   //update POTS the rest of the time
				
			
			while(clk<((Ntck*gate) >>8)){ // WAIT FOR END OF GATE
				//ENJOY SOUND HERE
			}
			if(!( (bpss & (note_nxt==0)) | ((gate>250)&(note_nxt==note_cur)) ) ){
			  /* NB: Note OFF sent EXCEPT if :
			   * i. (Bypass is ON) AND (next Knob is OFF)
			   * OR 
			   * ii. (Gate is MAX) AND (next note = current note)*/
				note_OFF(mco,note_snt,velo);
				stx_str("OFF:");
				stx_int(note_snt);
				stx_str("\r\n");
			} 
			set_OUT(&PRT_LED, PIN_LED, 0); //LED OFF
			
			checkRESET(); //push Button on rear panel
		}//END OF STEP

	}//while(1)		
}//main()


void update_CHRQB(void){ //update (ON-OFF-ON) Switches
	//TO DO: implement Source and Clock!!
	//uint8_t sw_S=check_SW(SW_S);
	//uint8_t sw_C=check_SW(SW_C);
	//NB : HOLD is on (S):1=ON;2=OFF
	clck = check_SW(SW_C);			// (C)lock
	hold = check_SW(SW_S);			// (H)old
	rate = getRate(); 				// (R)ate
	Ntck = (PPQN >> (rate>>3));
	qutz = getQuantize(); 			// (Q)uantize
	bpss = check_SW(SW_B); 			// (B)ypass
}

void update_TGV(void){ // update 8-bit potentiometers
	temp=getTempo();  		// (T)empo
	update_clk(temp);
	gate = getGate(); 		// (G)ate
	velo = getVelocity();	//	(V)elocity
}

uint8_t getNextNote(void){
	uint8_t note_1=0;
	r347=getR347(); // get next pot value
	stx_str("r347:");
	stx_int(note_snt);
	stx_str("\r\n");
	if (r347<99){note_1=root_cur+getR347()-12;}
	return note_1;
}

void checkRESET(void){
	if(check_SW(SW_R)){
		while(check_SW(SW_R)){
			_delay_ms(100);
		}
		//Reset parameters
		update_ALL();
	}
}

void update_ALL(void){
	step=7;
	set_ABC(step);
	update_CHRQB();
	update_TGV();
	getNextNote();
	clk=Ntck;
	
}
	
uint8_t eeprom_read(const uint8_t *b){ 
	//modified eeprom_read_byte /w default value at 1 instead of 255
	uint8_t c=eeprom_read_byte(b);
	if(c==255){c=1;}
	return c;
}		
		
void getMidiSetup(void){
	
	mco=eeprom_read(&MCO);//channel OUT
	mci=eeprom_read(&MCI);//channel IN
	src=eeprom_read(&SRC);//Midi Source

	if(check_SW(SW_R)){ //enter setup Mode
		
		
		src=check_SW(SW_S); //Check Midi Source
		
		while(check_SW(SW_R)){
			
			set_OUT(&PRT_LED, PIN_LED, 1);
			_delay_ms(250);
			set_OUT(&PRT_LED, PIN_LED, 0);
			_delay_ms(250);
			
			//enter CHANNEL Mode
			set_MPX(CH_T); //read TEMPO pot
			uint8_t ch=(read_ADC_8bit()>>4)+1; //0-16
			set_ABC(ch-1); // Set LED Index
			if(check_SW(SW_C)){
				mci=ch;
				stx_str("ch_IN: ");
				stx_int(ch);
				stx_str("\r\n");
				}
			else{
				mco=ch;
				stx_str("ch_OUT: ");
				stx_int(ch);
				stx_str("\r\n");
			}
			
			
			if(check_SW(SW_S)!=src) { // SW_S has changed
				src=!src; //update src
				if(src){ //S=1
					stx_str("SOURCE:");
					stx_int(1);
					stx_str("\r\n");
						}
				else{
					stx_str("SOURCE:");
					stx_int(2);
					stx_str("\r\n");
				}
			}
			
			
		}
		//set_OUT(&PRT_LED, PIN_LED, 0);
		eeprom_write_byte(&MCO,mco);
		eeprom_write_byte(&MCI,mci);
		eeprom_write_byte(&SRC,src);
	}
}
	

	

