/********************************************************************
 * file    : panel.h  (header for panel.c)
 * project : qr347 (8-step Midi Sequencer)
 * target  : ATmega328P (16MHz)
 * author  : ./qusw (https://github.com/qusvv)
 * license : GNU GPLv3 
 * 
 * Functions for reading Pot/Switch values on front panel
 *  
 * ./qusw
********************************************************************/


#ifndef PANEL_H
#define PANEL_H

#include <stdint.h>
#include "clock.h"
#include "adcmux.h"

// ADCs
#define CH_C 0  // 4051 COM
#define CH_T 1	// (T)empo
#define CH_Q 2	// (Q)uantize
#define CH_R 3	// (R)ate
#define CH_G 4	// (G)ate
#define CH_V 5	// (V)elocity

// ON-OFF switches
#define PRT_SW PIND 
#define DDR_SW DDRD
#define SW_C PD2	// (C)lock ( 0: Tempo / 1: MIDI )
#define SW_S PD3	// (S)ource ( 0: MIDI2 / 1: MIDI1 )
#define SW_B PD4	// "(B)ypass"= Hold ( 0: OFF / 1: ON)
#define SW_R PD7	// (R)eset  ( 0: idle / 1: pushed )

uint8_t getTempo(void);

uint8_t getRate(void);

uint8_t getGate(void);

uint8_t getVelocity(void);

uint8_t getQuantize(void);

uint8_t getR347(void);

void init_SW(void);

uint8_t check_SW(uint8_t swPin);



#endif /* PANEL_H */
