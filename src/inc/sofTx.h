/*****************************************************************
 * file    : sofTx.h  (Send-Only Software UART)
 * project : qr347 (8-step Midi Sequencer)
 * target  : ATmega328P (16MHz)
 * author  : ./qusw (https://github.com/qusvv)
 * license : GNU GPLv3
 * 
 * A minimal bit bang UA(R)T (Transmit Only) for ATmega328P
 * Largely inspired by SoftwareUART from Jeroen Lodder (2011)
 * 
 * ./qusw
*****************************************************************/
#ifndef SOFTX_H
#define SOFTX_H

#include "debug.h"
#include <avr/interrupt.h>
#include <avr/io.h>

#define START_BIT 1
#define DATA_BIT 2
#define STOP_BIT 3

#define STR_MAX	20   // Maximum length for Strings

#define PIN_TX PD6   //TX Pin
#define PRT_TX PORTD
#define DDR_TX DDRD

void init_TX(void);		 			// init software uart 

void stx_byte(uint8_t B);			//send single byte (8N1 bit bang routine)

void stx_int(int a);		// send integer (16-bit)
	
void stx_str(char *s);		//Send string (multiple bytes)

#endif /* SOFTX_H */



