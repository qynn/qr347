/****************************************************
 * file   : cd4051.h (header for cd4051.c)
 * project : qr347 (8-step Midi Sequencer)
 * target  : ATmega328P (16MHz)
 * author  : ./qusw (https://github.com/qusvv)
 * license : GNU GPLv3 
 * 
 *  LED/Pots multiplexer (cd4051)
 *  
 * ./qusw
****************************************************/
#ifndef CD4051_H
#define CD4051_H

#include <avr/io.h>
#include <stdio.h>

/* -----CD4051 multiplexers-------*/

//Shared Input states
#define PIN_A PB3   //input state A (1)
#define PIN_B PB2   //input state B (2)
#define PIN_C PB1   //input state C (4)
#define PRT_ABC PORTB
#define DDR_ABC DDRB
//LEDs
#define PIN_LED PB0   //LED COM (IN)
#define PRT_LED PORTB
#define DDR_LED DDRB
// Potentiometers 
#define PIN_COM	PC0	  //POT COM (OUT)
#define PRT_COM PORTC
#define DDR_COM DDRC


void init_4051(void);

void set_OUT(volatile uint8_t *port, uint8_t pin, uint8_t state);

void set_ABC(uint8_t i);



#endif /*CD4051_H*/
