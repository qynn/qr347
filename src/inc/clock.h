/*****************************************************************
 * file    : clock.h (header for clock.c)
 * project : qr347 (8-step Midi Sequencer)
 * target  : ATmega328P (16MHz)
 * author  : ./qusw (https://github.com/qusvv)
 * license : GNU GPLv3 
 * 
 * Midi IN / CLOCK generation for QR347
 *  
 * ./qusw
******************************************************************/
#ifndef CLOCK_H
#define CLOCK_H

#include <avr/interrupt.h>
#include <avr/io.h>
#include <stdio.h>

#define PPQN 96 // Pulses per Quarter Note
#define TEMPO_MIN	30
#define TEMPO_MAX	160

/* -----INTERNAL CLOCK-------*/

void init_timer0(void);

void update_clk(uint8_t tempo);

#endif /*CLOCK_H*/
