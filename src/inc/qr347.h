/**************************************************************
 * file    : qr347.h  (main header file)
 * project : qr347 (8-step Midi Sequencer)
 * target  : ATmega328P (16MHz)
 * author  : ./qusw (https://github.com/qusvv)
 * license : GNU GPLv3
 * 
 * ./qusw
***************************************************************/
#ifndef QR347_H
#define QR347_H

#include "clock.h"
#include "cd4051.h"
#include "adcmux.h"
#include "sofTx.h"
#include "panel.h"
#include "midi.h"

#define ON 1
#define OFF 0

void update_CHRQB(void); //update Switches

void update_TGV(void); //update Pots

void update_ALL(void); // Reset parameters

uint8_t getNextNote(void); 

void checkRESET(void);

void getMidiSetup(void);

uint8_t eeprom_read(const uint8_t *b); //modified eeprom_read_byte()



#endif /*CLOCK_H*/
