/**********************************************************
 * file    : midi.h  (header for midi.c)
 * project : qr347 (8-step Midi Sequencer)
 * target  : ATmega328P (16MHz)
 * author  : ./qusw (https://github.com/qusvv)
 * license : GNU GPLv3 
 * 
 * Midi implementation
 *  
 * ./qusw
************************************************************/


#ifndef MIDI_H
#define MIDI_H

#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <avr/io.h>


//Default Midi Status associated w/ Channel 1
#define NOTE_ON		144
#define NOTE_OFF	128
#define CTRL_CG		176

/*replacement of loop_until_bit_is_set()*/
#define wait_for_flag(port, bitnum) \
while ( ! ( (port) & (1 << (bitnum)) ) ) ;

void init_uart(void);

void send_byte(uint8_t b);

void send_midi(uint8_t MIDIcmd, uint8_t MIDInot, uint8_t MIDIval);

uint8_t note_ON(uint8_t MIDIch, uint8_t MIDInot, uint8_t MIDIvel);

void note_OFF(uint8_t MIDIch, uint8_t MIDInot, uint8_t MIDIvel);



#endif /* MIDI_H */
